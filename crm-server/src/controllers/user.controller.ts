import { Request, Response } from 'express';
import * as crypto from 'crypto';
import * as jwt from 'jsonwebtoken';
import db from '@models/index';
import { code, message } from '@utils';


export async function register(req: Request, res: Response) {
    try {
        let { email, username, password } = req.body;
        let checkUserEmail = await db.User.findOne({ where: { email } });
        if (checkUserEmail) return res.json({
            status: 400,
            code: code.default.CODE_REGISTER_EMAIL_EXSITED_ERROR,
            message: message.default.MESSAGE_REGISTER_FAIL,
            data: null
        })

        let checkUserName = await db.User.findOne({ where: { username } });
        if (checkUserName) return res.json({
            status: 400,
            code: code.default.CODE_REGISTER_USERNAME_EXSITED_ERROR,
            message: message.default.MESSAGE_REGISTER_FAIL,
            data: null
        })
        let salt_key: string = crypto.createHash('sha256').update(JSON.stringify(email + username + Date.now())).digest('base64')
        let pwd: string = crypto.createHmac('sha256', salt_key).update(JSON.stringify(password)).digest('hex');
        req.body.password = pwd;
        req.body.salt_key = salt_key;
        let user = await db.User.create(req.body);
        return res.json({
            status: 200,
            code: code.default.CODE_SUCCESS,
            message: message.default.MESSAGE_SUCCESS,
            data: user
        })
    } catch (error) {
        console.log(error)
        return res.json({
            status: 500,
            code: code.default.CODE_INTERNAL_SERVER_ERROR,
            message: message.default.MESSAGE_INTERNAL_SERVER_ERROR,
            data: error
        })
    }
}

export async function login(req: Request, res: Response) {
    try {
        let { username, password } = req.body;
        let user = await db.User.findOne({ where: { username } })
        if (!user) return res.json({
            status: 400,
            code: code.default.CODE_LOGIN_USERNAME_NOT_EXSITED_ERROR,
            message: message.default.MESSAGE_LOGIN_USERNAME_NOT_EXSITED,
            data: null
        })
        let pwd: string = crypto.createHmac('sha256', user.salt_key).update(JSON.stringify(password)).digest('hex');
        if (pwd !== user.password) return res.json({
            status: 400,
            code: code.default.CODE_LOGIN_PASSWORD_NOT_MATCHED_ERROR,
            message: message.default.MESSAGE_LOGIN_PASSWORD_NOT_MATCHED,
            data: null
        })
        let { id, email, name, role } = user;
        let payload = { id, username, name, email, role };
        let token = jwt.sign(payload,
            process.env.SECRET_JWT_KEY || 'SECRET_JWT_KEY')
        return res.json({
            status: 200,
            code: code.default.CODE_SUCCESS,
            message: message.default.MESSAGE_SUCCESS,
            data: { payload, token }
        })
    } catch (error) {
        console.log(error)
        return res.json({
            status: 500,
            code: code.default.CODE_INTERNAL_SERVER_ERROR,
            message: message.default.MESSAGE_INTERNAL_SERVER_ERROR,
            data: error
        })
    }
}