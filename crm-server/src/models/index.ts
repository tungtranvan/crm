import { Sequelize } from 'sequelize';
import { UserInstance } from './user.model';
let {
    DB_NAME = 'crm_dev',
    DB_HOST = 'localhost',
    DB_USER = 'root',
    DB_PASS = ''
} = process.env;
const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
    host: DB_HOST,
    dialect: 'mariadb'
});
const db = {
    sequelize,
    User: UserInstance(sequelize)
}
sequelize.sync();
export default db;