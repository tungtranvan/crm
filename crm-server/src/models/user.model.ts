import { Model, Sequelize, DataTypes } from 'sequelize';

export class IUser extends Model {
    public id!: number;
    public username!: string;
    public email!: string; //Email của người dùng
    public password!: string;
    public name!: string | null;
    public salt_key!: string | null;
    public role: string = 'user';
    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}

export function UserInstance(sequelize: Sequelize): any {
    IUser.init({
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true
        },
        username: {
            type: DataTypes.STRING(64),
            allowNull: false,
            unique: true
        },
        email: {
            type: DataTypes.STRING(64),
            allowNull: false,
            unique: true
        },
        name: {
            type: DataTypes.STRING(64),
            allowNull: false,
        },
        password: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        salt_key: {
            type: DataTypes.STRING(64),
            allowNull: false
        },
        role: {
            type: DataTypes.STRING(64),
            allowNull: false,
            defaultValue: 'user'
        }
    }, {
        tableName: 'users',
        sequelize: sequelize,
    })
    return IUser;
}