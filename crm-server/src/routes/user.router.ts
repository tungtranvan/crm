import { Request, Response, Router } from 'express';
import { userController } from '@controllers/index';
import { middewares } from '@utils';
const router = Router();

router.post('/register',
    middewares.requireParams(['username', 'email', 'password', 'name']),
    userController.register);

router.post('/login',
    middewares.requireParams(['username', 'password']),
    userController.login);

export default router;
