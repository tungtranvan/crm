const CODE = {
    CODE_SUCCESS: "C000",
    CODE_INTERNAL_SERVER_ERROR: "C001",
    CODE_REGISTER_EMAIL_EXSITED_ERROR: "C002",
    CODE_REGISTER_USERNAME_EXSITED_ERROR: "C003",
    CODE_MISSING_REQUIRE_PARAMETERS_ERROR: "C004",
    CODE_LOGIN_USERNAME_NOT_EXSITED_ERROR: "C005",
    CODE_LOGIN_PASSWORD_NOT_MATCHED_ERROR: "C006",
}

export default CODE;
