const MESSAGE = {
    MESSAGE_SUCCESS: "Success.",
    MESSAGE_INTERNAL_SERVER_ERROR: "Something was wrong.",
    MESSAGE_REGISTER_FAIL: "Register has failure.",
    MESSAGE_MISSING_REQUIRED_PARAMETERS: "Missing some required parameters: ",
    MESSAGE_LOGIN_USERNAME_NOT_EXSITED: "Login fail! No username was founded",
    MESSAGE_LOGIN_PASSWORD_NOT_MATCHED: "Login fail! Password not match",
}

export default MESSAGE;
