import { Request, Response, NextFunction } from 'express';
import code from './code';
import messages from './messages';

export function requireParams(params: Array<string> = []) {
    return function (req: Request, res: Response, next: NextFunction) {
        let { method } = req;
        let paramsReceived: any;
        let missingParams: string[] = [];
        if (method === 'POST')
            paramsReceived = req.body;
        if (method === 'GET') {
            paramsReceived = req.query;
        }
        params.forEach(param => {
            if (!paramsReceived.hasOwnProperty(param)) missingParams.push(param);
        })
        if (missingParams.length > 0)
            return res.json({
                status: 401,
                code: code.CODE_MISSING_REQUIRE_PARAMETERS_ERROR,
                message: messages.MESSAGE_MISSING_REQUIRED_PARAMETERS + missingParams,
                data: []
            })
        next();
    }
}