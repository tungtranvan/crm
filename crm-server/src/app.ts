import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import path from 'path';
import helmet from 'helmet';
import compression from 'compression';
import express, { Request, Response, NextFunction } from 'express';
import { BAD_REQUEST } from 'http-status-codes';
import 'express-async-errors';

import BaseRouter from './routes';
import db from '@models/index';
import * as swaggerUi from 'swagger-ui-express';
const swaggerDocument = require('./swagger/swagger.json');
// Init express
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.use('/api-docs', swaggerUi.serve);
app.get('/api-docs', swaggerUi.setup(swaggerDocument));
//Connect to database
db.sequelize.authenticate()
    .then(async () => {
        console.log("Connect to database successfully!");
    })
    .catch((error: Error) => {
        throw new Error(error.message);
    })


app.use(morgan('dev'));

// Security
if (process.env.NODE_ENV === 'production') {
    app.use(helmet());
    app.use(compression())
    app.use(express.static(path.resolve('dist/public')))
}

// Add APIs
app.use('/api', BaseRouter);
app.use(express.static('public'))

// Print API errors
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    console.error(err.message, err);
    return res.status(BAD_REQUEST).json({
        error: err.message,
    });
});

const viewsDir = path.join(__dirname, 'views');
app.set('views', viewsDir);
app.get('/', (req: Request, res: Response) => {
    res.sendFile('index.html', { root: viewsDir });
});
// Export express instance
export default app;
