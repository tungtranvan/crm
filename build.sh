#!/bin/sh

echo "Building source..."
BASEDIR=${PWD}
BEDIR=${PWD}'/crm-server'
FEDIR=${PWD}'/crm-frontend'
rm -rf ${BEDIR}/dist/
rm -rf ${FEDIR}/dist/
echo "Server code directory:" ${BEDIR}
echo "Frontend code directory:" ${FEDIR}

echo "Building server code..."
cd ${BEDIR}
npm run build
cp -r ${BEDIR}/src/swagger ${BEDIR}/dist/swagger
echo "Build server code done."

echo "Building frontend code..."
cd ${FEDIR}
ng build --prod
echo "Build frontend code done."

cp -rf ${FEDIR}/dist/frontend ${BEDIR}/dist/public
mkdir ${BEDIR}/dist/views
cp ${BEDIR}/dist/public/index.html ${BEDIR}/dist/views/